﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LigaDraft.Models.Custom
{
    public class Liga
    {
        [Key]
        public int LigaId { get; set; }

        [StringLength(10)]
        public string Name { get; set; }

    }
}