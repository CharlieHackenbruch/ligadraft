﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LigaDraft.Startup))]
namespace LigaDraft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
