﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using LigaDraftTests.Selenium;
using OpenQA.Selenium.Support.UI;

namespace LigaDraftTests
{
    [TestClass]
    public class HomeTests
    {
        [TestInitialize]
        public void TestInitialization()
        {
        }

        [TestMethod]
        public void LandingHome()
        {
            Automation.MyDriver.Navigate().GoToUrl("http://ligadraft20180314114008.azurewebsites.net/");
            Automation.Wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".container")));
            Assert.IsTrue(Automation.MyDriver.FindElements(By.CssSelector(".jumbotron")).Count == 1);
        }
    }
}
