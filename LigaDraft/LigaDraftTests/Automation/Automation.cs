﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace LigaDraftTests.Selenium
{
    public static class Automation
    {
        public static IWebDriver MyDriver { get; }

        public static WebDriverWait Wait { get; }
        
        static Automation()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("-incognito");
            options.AddArgument("disable-infobars");
            options.AddArgument("--start-maximized");
            MyDriver = new ChromeDriver(options);

            Wait = new WebDriverWait(MyDriver, TimeSpan.FromSeconds(45));
        }
    }
}
